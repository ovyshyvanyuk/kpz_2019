﻿using System;
using System.Collections.Generic;
using Robot.Common;

namespace Vyshyvanyuk.Oleg.RobotChallange
{
    class VyshyvanyukOlegAlgorithm : IRobotAlgorithm
    {
        public string Author
        {
            get
            {
                return "Vyshyvanyuk Oleg";
            }
        }

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
            if ((movingRobot.Energy > 500) && (robots.Count < map.Stations.Count))
                return new CreateNewRobotCommand() { NewRobotEnergy = 200 };
            Position stationPosition = DistanceHelper.FindNearestFreeStation(robots[robotToMoveIndex], map, robots);
            if (stationPosition == null)
                return null;

            if (stationPosition == movingRobot.Position)
                return new CollectEnergyCommand();
            else
                return new MoveCommand() { NewPosition = stationPosition };
        }
    }
}
